#!/usr/bin/python

import serial
from time import sleep
import base64
import zlib
from kiss import Decoder
from datetime import datetime
import sys

ser = serial.Serial("/dev/ttyS3",115200)
photoDecoder = Decoder()
data_packs = bytes()

serialNotFinish = 1
cmdTrigger = "gotcha!"

while serialNotFinish:
	data_packs += ser.read()
	sleep(0.05)
	serialLeft = ser.inWaiting()
	data_packs += ser.read(serialLeft)

	sleep(0.05)
	if ser.inWaiting() == 0:
		serialNotFinish = 0

print('Data received')
photoData = photoDecoder.apply(data_packs)
print('Data decoded')

compressedStr =''.join(map(str, photoData))
photoStr = compressedStr 

photoStr = photoStr[(photoStr.find('bytearray')+12):(len(photoStr)-2)]	#Hardcoded!!
print('Data translated')

photoName = sys.argv[1]
photoFullName = photoName + ".jpg"

imageFile = open(photoFullName,"wb")
imageFile.write(base64.b64decode(photoStr + '='))
imageFile.close()


print('Done!')
sleep(3)
ser.close()

