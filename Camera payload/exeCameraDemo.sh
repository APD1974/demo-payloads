#!/bin/bash

cd /oc/bin/api
./zmq_client power user p 5v0 1
./zmq_client leds user i 0
./zmq_client leds user s 0 1
./zmq_client leds user s 1 0
./zmq_client leds user s 2 0
./zmq_client leds user s 3 0
./zmq_client leds user s 4 0

sleep 45

./zmq_client leds user s 1 1

cd /home/OC_demos
imageName=photoDemo_$(date +"%y-%m-%d_%H:%M:%S")
./CameraDemoSerialBK.py $imageName &
cd /oc/bin/api

sleep 5
echo "takeLDphoto" > /dev/ttyS3

./zmq_client leds user s 2 1
sleep 1

./zmq_client leds user s 3 1
echo "takeLDphoto" > /dev/ttyS3


while ! find /home/OC_demos -name *$imageName* -printf 1 | grep -q 1
do    
	sleep 1
done

./zmq_client leds user s 4 1
sleep 10


./zmq_client power user p 5v0 0
kill $(jobs -p)
sleep 2

./zmq_client leds user s 0 0
./zmq_client leds user s 1 0
./zmq_client leds user s 2 0
./zmq_client leds user s 3 0
./zmq_client leds user s 4 0
sleep 0.5
./zmq_client leds user s 0 1
./zmq_client leds user s 1 1
./zmq_client leds user s 2 1
./zmq_client leds user s 3 1
./zmq_client leds user s 4 1
sleep 0.5
./zmq_client leds user s 0 0
./zmq_client leds user s 1 0
./zmq_client leds user s 2 0
./zmq_client leds user s 3 0
./zmq_client leds user s 4 0
