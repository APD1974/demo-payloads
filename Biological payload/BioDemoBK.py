#!/usr/bin/python

from smbus2 import SMBus
import time
import datetime
import subprocess 

ZmqAddress = "/oc/bin/api/zmq_client"
CmdZmq5vOn = "/oc/bin/api/zmq_client power user p 5v0 1"
CmdZmqHvOn = "/oc/bin/api/zmq_client power user p hv 1"
CmdZmq5vOff = "/oc/bin/api/zmq_client power user p 5v0 0"
CmdZmqHvOff = "/oc/bin/api/zmq_client power user p hv 0"
CmdZmqHvSet = "/oc/bin/api/zmq_client power user v %d"
CmdZmqLedSet = "/oc/bin/api/zmq_client leds user s %d %d"
CmdZmqBattLowGet = "/oc/bin/api/zmq_client registers user g battery_low"
CmdZmqBattLowSet = "/oc/bin/api/zmq_client registers user s battery_low %d"

messageSize = 6
LoopTime = 3
photoTaken = False
battLow = False
addr = 0x8
bootUpTime = 60


subprocess.Popen(CmdZmqLedSet % (0,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (1,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (2,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (3,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (4,0), shell=True)
subprocess.Popen(CmdZmqBattLowSet % 0, shell=True)	

startTime = time.time()
dateTime = datetime.datetime.now()
dateTimeStr = dateTime.strftime("%Y-%m-%d_%H:%M:%S")
logFileName = "bioLog" + dateTimeStr + ".txt"

subprocess.Popen(CmdZmqLedSet % (0,1), shell=True)
bus = SMBus(1)

subprocess.Popen(CmdZmq5vOn, shell=True)
time.sleep(5)
subprocess.Popen(CmdZmqLedSet % (1,1), shell=True)


while (not battLow):

	PlOutput = bus.read_i2c_block_data(addr,0,messageSize)
	PlTemp = PlOutput[0] + (PlOutput[1] / 100) 
	PlControl = PlOutput[2] * 1000
	PlLed1 = PlOutput[3]
	PlLed2 = PlOutput[4]
	PlLed3 = PlOutput[5]
	print('Temperature = %.2f' % (PlTemp))
	print('PlControl = %i' % (PlControl))
	
	now = datetime.datetime.now() 
	nowStr = now.strftime("%H%M%S")

	with open(logFileName,'a') as file:
		file.write(nowStr)
		file.write(',')
		file.write(str(PlTemp))
		file.write(',')
		file.write(str(PlControl))
		file.write(',')
		file.write(str(PlLed1))
		file.write(',')
		file.write(str(PlLed2))
		file.write(',')
		file.write(str(PlLed3))
		file.write(';\n')
		file.close()
		
	if (PlControl == 0):
		subprocess.Popen(CmdZmqHvOff, shell=True)
		subprocess.Popen(CmdZmqLedSet % (2,0), shell=True)
	else:
		subprocess.Popen(CmdZmqHvOn, shell=True)
		subprocess.Popen(CmdZmqLedSet % (2,1), shell=True)
		subprocess.Popen(CmdZmqHvSet % (PlControl), shell=True)	

	timeCount = time.time() - startTime 
	if ((not photoTaken) and (timeCount > bootUpTime)):
		photoNameStr = "photoBio" + now.strftime("%Y-%m-%d_%H:%M:%S")
		print("/home/OC_demos/CameraDemoSerialBK.py " + photoNameStr)
		print("Smile!")
		subprocess.Popen("/home/OC_demos/CameraDemoSerialBK.py " + photoNameStr, shell=True)
		time.sleep(3)
		subprocess.Popen("echo takeLDphoto -> /dev/ttyS3", shell=True)
		photoTaken = True	
		subprocess.Popen(CmdZmqLedSet % (3,1), shell=True)

	battStatus = subprocess.run(CmdZmqBattLowGet, shell=True, stdout=subprocess.PIPE)	
	if ('1' in battStatus.stdout.decode("utf-8")):
		battLow = True
		
	time.sleep(LoopTime)


print("closing...")
time.sleep(5)

subprocess.Popen(CmdZmqLedSet % (0,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (1,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (2,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (3,0), shell=True)
subprocess.Popen(CmdZmqLedSet % (4,0), shell=True)

subprocess.Popen(CmdZmqHvOff, shell=True)
subprocess.Popen(CmdZmq5vOff, shell=True)	
